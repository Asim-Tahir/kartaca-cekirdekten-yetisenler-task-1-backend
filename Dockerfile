FROM golang:alpine as single-stage
WORKDIR /app
RUN apk add g++
COPY . .

RUN go build -o server /app/cmd/server/
ENV PORT 4000
EXPOSE 4000
ENTRYPOINT [ "/app/server" ]